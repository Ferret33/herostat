function representsDate(text) {
  return (text.slice(4, 5) === '-') && (text.slice(7, 8) === '-') && (text.slice(10, 11) === 'T');
}

var table = document.getElementById('g_tbl');
var position = 'https://godville.net/gods/'.length;
var people = new Array();
for (var i = 1; i < table.rows.length; i++) {
  var element = table.rows[i].cells[1];
  var href = element.childNodes[0].href;
  var address = href.slice(0, position) + 'api/' + href.slice(position);
  people.push(address);
}

var daddy = table.parentElement;
var statTable = document.createElement('table');
var tr = document.createElement('tr');
var th;
var headers = ['Имя',  'Звание',     'Лвл', 'Торг', 'Храм',             'Кирпичей', 'Ковчег',        'Брёвен', 'Сбер', 'Самцов', 'Самок', 'Дуэль+', 'Дуэль-'];
//              godname clan_position level  t_level temple_completed_at bricks_cnt  ark_completed_at wood_cnt  savings ark_m     ark_f   arena_won  arena_lost
var props = ['godname', 'clan_position', 'level', 't_level', 'temple_completed_at', 'bricks_cnt', 'ark_completed_at',
            'wood_cnt', 'savings', 'ark_m', 'ark_f', 'arena_won', 'arena_lost'];
for (var i = 0; i < headers.length; i++) {
  th = document.createElement('th');
  th.innerHTML = headers[i];
  tr.appendChild(th);
}

statTable.appendChild(tr);
daddy.insertBefore(statTable, table);

var curInd = 0;
var xhr = new XMLHttpRequest();
var td;
var xhrResult;
var curText;
var curType;
var curValue;
var thousandsPos;
xhr.onreadystatechange = function () {
  if (xhr.readyState !== 4) return;
  //console.log(xhr.responseText);
  if (typeof xhr.response === 'object') {
    xhrResult = xhr.response;
  } else {
    xhrResult = JSON.parse(xhr.responseText);
  }

  tr = document.createElement('tr');
  for (var i = 0; i < props.length; i++) {
    curValue = xhrResult[props[i]];
    curType = typeof curValue;
    switch (curType) {
      case 'undefined':
        curText = '-';
        break;
      case 'object':
        if (curValue == null) {
          curText = '-';
          break;
        }
        if (curValue instanceof Date) {
          curText = curValue.toLocaleDateString('ru');
        } else {
          curText = curValue.toString();
        }
        break;
      case 'string':
        if (representsDate(curValue)) {
          curValue = new Date(curValue);
          console.log('Date: ' + curValue + ' ' + typeof curValue);
          curText = curValue.toLocaleDateString('ru');
          break;
        }
        thousandsPos = curValue.indexOf(' тыс');
        if (thousandsPos < 0) {
          curText = curValue;
        } else {
          curText = curValue.slice(0, thousandsPos);
        }
        break;
      default:
        curText = curValue;
    }
    td = document.createElement('td');
    td.innerHTML = curText;
    tr.appendChild(td);
  }

  statTable.appendChild(tr);
}

var timerId;

function onTimer() {
  xhr.open('GET', people[curInd]);
  xhr.send(null);

  curInd++;
  if (curInd >= people.length) {
    clearInterval(timerId);
  }
}

if (people.length > 0) {
  timerId = setInterval(onTimer, 60000);
  onTimer();
}
